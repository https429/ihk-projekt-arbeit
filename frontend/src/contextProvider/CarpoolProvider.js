import {createContext, useEffect, useState} from "react"
import {fetchJson, sendJson} from "../FetchRequests"

export const CarpoolContext = createContext()

const CarpoolProvider = ({children}) => {
    const [carpoolList, setCarpoolList] = useState([])
    const [ownCarpoolList, setOwnCarpoolList] = useState([])
    const [selectedCarpool, setSelectedCarpool] = useState(null)
    const [carpoolFilterQuery, setCarpoolFilterQuery] = useState("")
    const [isUpdating, setUpdating] = useState(false)

    useEffect(() => {
        update()
    }, [])

    useEffect(() => {
        filterCarpools(carpoolFilterQuery)
        // eslint-disable-next-line
    }, [carpoolFilterQuery])

    const update = async () => {
        setUpdating(true)
        const allCarpoolResponse = await fetchJson("api/v1/carpool")
        const ownCarpoolResponse = await fetchJson("api/v1/carpool?own=true")

        if (allCarpoolResponse.ok) {
            setCarpoolList(await allCarpoolResponse.json())
        } else throw allCarpoolResponse

        if (ownCarpoolResponse.ok) {
            setOwnCarpoolList(await ownCarpoolResponse.json())
        } else throw ownCarpoolResponse

        setUpdating(false)
    }

    const addCarpool = async (label, streetName, streetNumber, postalCode, city) => {
        const response = await sendJson("POST", "api/v1/carpool", {
            label,
            streetName,
            streetNumber,
            postalCode,
            city
        })
        if (response.ok) {
            await update()
            return true
        }
        if (response.status === 400) return await response.json()

        throw response
    }

    const deleteCarpool = async carpoolID => {
        const response = await sendJson("DELETE", `api/v1/carpool/${carpoolID}`)
        if (response.ok) {
            await update()
            return true
        }
        throw response
    }

    const changeStatusCarpool = async (carpoolID, status) => {
        const response = await sendJson("PUT", `api/v1/carpool/${carpoolID}`, {
            active: status
        })
        if (response.ok) {
            await update()
            return true
        }

        throw response
    }

    const filterCarpools = query => {
        query = query.trim().toLowerCase()

        const copyList = carpoolList.map(carpool => {
            const {destination, driver} = carpool
            let match = false

            if (!query || query.length === 0) {
                carpool.match = true
            } else {
                if (!match && destination.label.toLowerCase().indexOf(query) !== -1) match = true
                if (!match && destination.streetName.toLowerCase().indexOf(query) !== -1) match = true
                if (!match && destination.postalCode.toLowerCase().indexOf(query) !== -1) match = true
                if (!match && driver.displayName.toLowerCase().indexOf(query) !== -1) match = true

                carpool.match = match
            }
            return carpool
        })

        setCarpoolList(copyList)
    }

    const selectCarpool = carpool => {
        if (carpool === selectedCarpool) {
            setSelectedCarpool(null)
        } else {
            setSelectedCarpool(carpool)
        }
    }

    return (
        <CarpoolContext.Provider
            value={{
                carpoolList,
                ownCarpoolList,
                updateCarpoolLists: update,
                isUpdating,
                addCarpool,
                deleteCarpool,
                changeStatusCarpool,
                filterCarpools,
                selectCarpool,
                selectedCarpool,
                carpoolFilterQuery,
                setCarpoolFilterQuery,
            }}
        >
            {children}
        </CarpoolContext.Provider>
    )
}

export default CarpoolProvider