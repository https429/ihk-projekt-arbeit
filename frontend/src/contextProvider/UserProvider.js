import React, {createContext, useState} from "react"
import {fetchJson, sendJson} from "../FetchRequests"

export const UserContext = createContext()

const UserProvider = ({children}) => {
    const [isAuthenticated, setAuthenticated] = useState(false)
    const [user, setUser] = useState(null)

    /* True: Wenn angemeldet
     * False: Wenn Passwort/ Username falsch
     * Error throw: Wenn Fehler auftrat
     */
    const login = async (username, password) => {
        const response = await sendJson('POST', 'api/v1/login', {
            username: username,
            password: password,
        })
        if (response.ok) {
            setUser(await response.json())
            setAuthenticated(true)
            return true
        }
        setAuthenticated(false)
        setUser(null)
        if (response.status === 401) return false
        throw response
    }

    // True: Wenn alles klappt, Error throw bei fehler
    const logout = async () => {
        const response = await fetchJson('api/v1/logout')
        // Ist alles glatt gegangen oder war der User gar nicht erst angemeldet
        if (response.ok || response.status === 401) {
            setAuthenticated(false)
            setUser(false)
            return true
        }

        throw response
    }

    /* True: Wenn alles glatt läuft
     * Fehler Message: Wenn Addresse nicht gefunden werden kann
     * Throw Error: Bei unbekannter fehler
     */
    const setAddress = async (streetName, streetNumber, postalCode, city) => {
        const response = await sendJson('PUT', 'api/v1/user', {
            streetName,
            streetNumber,
            postalCode,
            city
        })
        if (response.ok) {
            setUser(await response.json())
            return true
        }

        if (response.status === 400) return await response.json()
        throw response
    }

    return (
        <UserContext.Provider
            value={{
                isAuthenticated: isAuthenticated,
                setAuthenticatedFalse: () => setAuthenticated(false),
                userObj: user,
                login,
                logout,
                setAddress,
            }}
        >
            {children}
        </UserContext.Provider>
    )
}

export default UserProvider