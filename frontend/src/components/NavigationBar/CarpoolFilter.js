import {Form, FormControl} from "react-bootstrap"
import styles from "./CarpoolFilter.module.css"
import {useContext} from "react";
import {CarpoolContext} from "../../contextProvider/CarpoolProvider";

const CarpoolFilter = () => {
    const {carpoolFilterQuery, setCarpoolFilterQuery} = useContext(CarpoolContext)

    return (
        <Form
            className={styles.destinationForm}
            inline
            onSubmit={event => event.preventDefault()}
        >
            <FormControl
                type={"text"}
                placeholder={"Fahrtziel"}
                className={"w-100"}
                value={carpoolFilterQuery}
                onChange={event => setCarpoolFilterQuery(event.target.value)}
            />
        </Form>
    )
}

export default CarpoolFilter