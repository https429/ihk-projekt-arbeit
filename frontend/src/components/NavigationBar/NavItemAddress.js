import {useContext, useState} from "react"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {Dropdown, NavItem} from "react-bootstrap"
import {UserContext} from "../../contextProvider/UserProvider";
import AddressModal from "./Modal/AddressModal";

const NavItemAddress = () => {
    const [showAddressModal, setShowAddressModal] = useState(false)
    const {userObj, logout} = useContext(UserContext)

    return (<>
        <Dropdown as={NavItem}>
            <Dropdown.Toggle variant={"light"}>
                <FontAwesomeIcon icon={["fas", "user"]}/>
                {userObj.displayName}
            </Dropdown.Toggle>
            <Dropdown.Menu className={"dropdown-menu-right"}>
                <Dropdown.Item
                    onClick={() => setShowAddressModal(true)}
                >Adresse</Dropdown.Item>
                <Dropdown.Item
                    onClick={logout}
                >Abmelden</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
        <AddressModal
            show={showAddressModal}
            handleClose={() => setShowAddressModal(false)}
        />
    </>)
}

export default NavItemAddress