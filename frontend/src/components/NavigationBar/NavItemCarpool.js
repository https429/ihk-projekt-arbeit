import React, {useState} from "react"
import {NavItem, Button} from "react-bootstrap"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import CarpoolModal from "./Modal/CarpoolModal";

const NavItemCarpool = () => {
    const [showCarpoolModal, setShowCarpoolModal] = useState(false)

    return (<>
        <NavItem
            as={Button}
            variant={"light"}
            onClick={() => setShowCarpoolModal(true)}
        >
            <FontAwesomeIcon icon={["fas", "taxi"]}/>
            Fahrgemeinschaften
        </NavItem>
        <CarpoolModal
            show={showCarpoolModal}
            handleClose={() => setShowCarpoolModal(false)}
        />
    </>)
}

export default NavItemCarpool