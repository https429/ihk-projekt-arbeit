import React from "react"
import {Alert} from "react-bootstrap"

const ModalAlertComponent = ({errorMessage, close}) => {

    return (
        <Alert
            variant={"danger"}
            className={"w-75 mt-2 mb-0 mx-auto"}
            dismissible
            onClose={close}
            show={errorMessage !== ""}
            transition={false}
        >
            {errorMessage}
        </Alert>
    )
}

export default ModalAlertComponent