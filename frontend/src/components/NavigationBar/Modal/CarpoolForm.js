import React, {useContext, useState} from "react"
import {Alert, Button, Col, Form, FormControl} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useForm} from "react-hook-form";
import LoadingSpinner from "../../LoadingSpinner";
import {CarpoolContext} from "../../../contextProvider/CarpoolProvider";

const CarpoolForm = () => {
    const {register, handleSubmit, formState: {errors}, reset} = useForm()
    const [formError, setFormError] = useState(null)
    const [isLoading, setLoading] = useState(false)
    const {addCarpool} = useContext(CarpoolContext)


    const onSubmit = ({label, streetName, streetNumber, postalCode, city}) => {
        setLoading(true)
        addCarpool(label, streetName, streetNumber, postalCode, city)
            .then(value => {
                setLoading(false)
                if (value === true) {
                    setFormError(null)
                    reset()
                } else {
                    setFormError(value)
                }
            }).catch(err => {
            setLoading(false)
            setFormError(err)
        })
    }

    return (
        <Form
            onSubmit={handleSubmit(onSubmit)}
        >
            <fieldset disabled={isLoading}>
                {formError
                && <Alert variant={"danger"}>{formError?.message}</Alert>
                }
                <Form.Row className={"mb-2"}>
                    <Col>
                        <Form.Control
                            placeholder={"Fahrtziel"}
                            type={"text"}
                            required
                            {...register("label", {required: true})}
                        />
                    </Col>
                </Form.Row>
                <Form.Row className={"mb-2"}>
                    <Col md={9}>
                        <Form.Control
                            placeholder={"Straße"}
                            type={"text"}
                            required
                            {...register("streetName", {required: true})}
                        />
                    </Col>
                    <Col md={3}>
                        <Form.Control
                            placeholder={"Nummer"}
                            type={"text"}
                            required
                            isInvalid={errors.streetNumber}
                            {...register("streetNumber", {
                                required: true,
                                pattern: /^\d+\w?$/
                            })}
                        />
                        {errors.streetNumber?.type === "pattern"
                        &&
                        <Form.Control.Feedback type={"invalid"} tooltip>Bitte prüfe das Format</Form.Control.Feedback>
                        }
                    </Col>
                </Form.Row>
                <Form.Row className={"mb-2"}>
                    <Col md={4}>
                        <Form.Control
                            placeholder={"PLZ"}
                            type={"text"}
                            required
                            isInvalid={errors.postalCode}
                            {...register("postalCode", {
                                required: true,
                                pattern: /^\d{5}$/
                            })}
                        />
                        {errors.postalCode?.type === "pattern"
                        && <FormControl.Feedback type={"invalid"} tooltip>Bitte prüfe das Format</FormControl.Feedback>
                        }
                    </Col>
                    <Col md={8}>
                        <Form.Control
                            placeholder={"Ort"}
                            type={"text"}
                            required
                            {...register("city", {required: true})}
                        />
                    </Col>
                </Form.Row>
                <div className={"w-100 text-center pt-2"}>
                    <Button
                        variant={"dark"}
                        type={"submit"}
                        className={"btn-spacing"}
                    >
                        <LoadingSpinner isLoading={isLoading}>
                            <FontAwesomeIcon icon={["fas", "plus"]}/>
                            Hinzufügen
                        </LoadingSpinner>
                    </Button>
                </div>
            </fieldset>
        </Form>
    )
}

export default CarpoolForm