import React, {useContext, useState} from "react"
import {Button, Card, Col, Row} from "react-bootstrap"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {CarpoolContext} from "../../../contextProvider/CarpoolProvider";

const CarpoolModalDetail = ({carpool}) => {
    const {deleteCarpool, changeStatusCarpool} = useContext(CarpoolContext)
    const [isLoading, setLoading] = useState(false)

    const handleDelete = () => {
        setLoading(true)
        deleteCarpool(carpool.id)
            .then(() => setLoading(false))
    }

    const handleStatusChange = () => {
        setLoading(true)
        changeStatusCarpool(carpool.id, !carpool.active)
            .then(() => setLoading(false))
    }

    return (
        <Card className={"mb-3 w-100"}>
            <Card.Body>
                <Row>
                    <Col md={9}>
                        <Card.Title className={"h6 my-1 font-weight-bold"}>{carpool.destination.label}</Card.Title>
                        <p className={"mb-0"}>{carpool.destination.streetName} {carpool.destination.streetNumber}</p>
                        <p className={"mb-0"}>{carpool.destination.postalCode} {carpool.destination.city}</p>
                    </Col>
                    <Col md={3} align={"center"}>
                        <Button
                            variant={"light"}
                            disabled={isLoading}
                            onClick={handleDelete}
                        >
                            <FontAwesomeIcon icon={["fas", "trash"]}/>
                        </Button>
                        <Button
                            className={"mt-1"}
                            variant={carpool.active ? "success" : "dark"}
                            disabled={isLoading}
                            onClick={handleStatusChange}
                        >
                            {carpool.active ? "Aktiv" : "Inaktiv"}
                        </Button>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    )
}

export default CarpoolModalDetail