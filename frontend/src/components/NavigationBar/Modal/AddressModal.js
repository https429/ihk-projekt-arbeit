import React from "react"
import {Modal} from "react-bootstrap"
import AddressForm from "./AddressForm";

const AddressModal = ({show, handleClose}) => {
    return (
        <Modal
            show={show}
            onHide={handleClose}
            animation={false}
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title>Adresse bearbeiten</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <AddressForm handleClose={handleClose}/>
            </Modal.Body>
        </Modal>
    )
}

export default AddressModal