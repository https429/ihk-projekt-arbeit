import React, {useContext} from "react"
import {Modal} from "react-bootstrap"
import CarpoolForm from "./CarpoolForm";
import {CarpoolContext} from "../../../contextProvider/CarpoolProvider";
import CarpoolModalDetail from "./CarpoolModalDetail";
import styles from "./CarpoolModal.module.css"

const CarpoolModal = ({show, handleClose}) => {
    const {ownCarpoolList} = useContext(CarpoolContext)

    return (
        <Modal
            show={show}
            onHide={handleClose}
            animation={false}
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title>Fahrgemeinschaft anbieten</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <CarpoolForm />
            </Modal.Body>
            <Modal.Footer className={styles.detailList}>
                {ownCarpoolList.map(carpool => {
                   return <CarpoolModalDetail carpool={carpool} key={carpool.id} />
                })}
            </Modal.Footer>
        </Modal>
    )
}

export default CarpoolModal