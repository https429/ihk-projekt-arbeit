import React, {useContext, useState} from "react"
import {useForm} from "react-hook-form"
import {Alert, Button, Col, Form, FormControl} from "react-bootstrap"
import {UserContext} from "../../../contextProvider/UserProvider";
import LoadingSpinner from "../../LoadingSpinner";

const AddressForm = ({handleClose}) => {
    const {register, handleSubmit, formState: {errors}} = useForm()
    const [formError, setFormError] = useState(null)
    const [isLoading, setLoading] = useState(false)
    const {userObj: {address}, setAddress} = useContext(UserContext)

    const onSubmit = ({streetName, streetNumber, postalCode, city}) => {
        setLoading(true)
        setAddress(streetName, streetNumber, postalCode, city)
            .then(value => {
                setLoading(false)
                if (value === true) {
                    setFormError(null)
                    handleClose()
                } else {
                    setFormError(value)
                }
            }).catch(err => {
            setLoading(false)
            setFormError(err)
        })
    }

    return (
        <Form
            onSubmit={handleSubmit(onSubmit)}
        >
            <fieldset disabled={isLoading}>
                {formError
                && <Alert variant={"danger"}>{formError?.message}</Alert>
                }
                <Form.Row className={"mb-2"}>
                    <Col md={9}>
                        <Form.Control
                            placeholder={"Straße"}
                            type={"text"}
                            required
                            defaultValue={address?.streetName}
                            {...register("streetName", {required: true})}
                        />
                    </Col>
                    <Col md={3}>
                        <Form.Control
                            placeholder={"Nummer"}
                            type={"text"}
                            required
                            defaultValue={address?.streetNumber}
                            isInvalid={errors.streetNumber}
                            {...register("streetNumber", {
                                required: true,
                                pattern: /^\d+\w?$/
                            })}
                        />
                        {errors.streetNumber?.type === "pattern"
                        && <FormControl.Feedback type={"invalid"} tooltip>Bitte prüfe das Format</FormControl.Feedback>
                        }
                    </Col>
                </Form.Row>
                <Form.Row>
                    <Col md={4}>
                        <Form.Control
                            placeholder={"PLZ"}
                            type={"text"}
                            required
                            defaultValue={address?.postalCode}
                            isInvalid={errors.postalCode}
                            {...register("postalCode", {
                                required: true,
                                pattern: /^\d{5}$/
                            })}
                        />
                        {errors.postalCode?.type === "pattern"
                        && <FormControl.Feedback type={"invalid"} tooltip>Bitte prüfe das Format</FormControl.Feedback>
                        }
                    </Col>
                    <Col md={8}>
                        <Form.Control
                            placeholder={"Ort"}
                            type={"text"}
                            required
                            defaultValue={address?.city}
                            {...register("city", {required: true})}
                        />
                    </Col>
                </Form.Row>
                <Form.Row className={"mt-2"}>
                    <Button
                        variant={"outline-dark"}
                        onClick={handleClose}
                        className={"ml-auto mr-1"}
                    >Abbrechen</Button>
                    <Button
                        variant={"dark"}
                        type={"submit"}
                        className={"mr-1"}
                    >
                        <LoadingSpinner isLoading={isLoading}>Anwenden</LoadingSpinner>
                    </Button>
                </Form.Row>
            </fieldset>
        </Form>
    )
}

export default AddressForm