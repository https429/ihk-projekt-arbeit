import React from "react"
import {Nav, Navbar} from "react-bootstrap"
import NavItemCarpool from "./NavItemCarpool";
import NavItemAddress from "./NavItemAddress";
import CarpoolFilter from "./CarpoolFilter";
import "./NavigationBar.css"

const NavigationBar = () => {


    return (
        <Navbar bg={"light"}>
            <Navbar.Brand>BX-Drive</Navbar.Brand>
            <Navbar.Collapse className={"justify-content-end"}>
                <CarpoolFilter/>
                <Nav>
                    <NavItemCarpool/>
                    <NavItemAddress/>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default NavigationBar