import React from "react"
import styles from "./Screen.module.css"
import NavigationBar from "./NavigationBar/NavigationBar";
import CarpoolProvider from "../contextProvider/CarpoolProvider";
import CarpoolColumn from "./CarpoolList/CarpoolColumn";
import Map from "./Map/Map";

const Screen = () => {

    return (
        <CarpoolProvider>
            <div className={styles.screen}>
                <NavigationBar/>
                <div className={styles.content}>
                    <div className={styles.carpoolColumn}>
                        <CarpoolColumn/>
                    </div>
                    <div className={styles.mapColumn}>
                        <Map/>
                    </div>
                </div>
            </div>
        </CarpoolProvider>
    )
}

export default Screen