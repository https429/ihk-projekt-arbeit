import React, {useContext} from "react"
import {Button, Col, Row} from "react-bootstrap"
import {CarpoolContext} from "../../contextProvider/CarpoolProvider"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import CarpoolList from "./CarpoolList";
import styles from "./CarpoolColmn.module.css"

const CarpoolColumn = () => {
    const {updateCarpoolLists} = useContext(CarpoolContext)

    return (
        <div className={styles.wrapper}>
            <h2 className={"text-center"}>Fahrgemeinschaften</h2>
            <Button
                variant={"outline-dark"}
                className={"btn-block"}
                onClick={updateCarpoolLists}
            >
                <FontAwesomeIcon icon={["fas", "sync-alt"]}/>
                Aktualisieren
            </Button>
            <hr className={styles.hr}/>
            <CarpoolList/>
        </div>)
}

export default CarpoolColumn