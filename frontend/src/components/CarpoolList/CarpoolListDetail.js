import React, {useContext} from "react"
import styles from "./CarpoolListDetail.module.css"
import {Button, Card, Col, Row} from "react-bootstrap"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {CarpoolContext} from "../../contextProvider/CarpoolProvider";

const CarpoolListDetail = ({carpool}) => {
    const {selectCarpool, selectedCarpool} = useContext(CarpoolContext)

    return (
        <Card
            className={`mb-3 ${styles.carpoolListDetail}`}
            onClick={event => {
                if (event.target.attributes.type?.value === "button") return
                selectCarpool(carpool)
            }}
        >
            <div className={carpool === selectedCarpool? styles.selected : ''}/>
            <Card.Body className={`ml-2 py-0 ${styles.cardBody}`}>
                <Card.Title className={"h6 my-1 font-weight-bold"}>{carpool.driver.displayName}</Card.Title>
                <Row>
                    <Col xl={7}>
                        <p className={styles.destination}>{carpool.destination.label}</p>
                        <p>{carpool.destination.streetName} {carpool.destination.streetNumber}</p>
                        <p>{carpool.destination.postalCode} {carpool.destination.city}</p>
                    </Col>
                    <Col xl={5}>
                        <Button
                            variant={"dark"}
                            href={`mailto:${carpool.driver.email}?subject=Darf%20Ich%20bei%20dir%20mitfahren%3F`}
                            className={"btn-block my-1"}
                        >
                            <FontAwesomeIcon icon={["fas", "envelope"]} className={"mr-1"}/>
                            Kontakt
                        </Button>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    )
}

export default CarpoolListDetail