import React, {useContext, useEffect, useState} from "react"
import {CarpoolContext} from "../../contextProvider/CarpoolProvider"
import {Spinner} from "react-bootstrap"
import CarpoolListDetail from "./CarpoolListDetail"
import styles from "./CarpoolList.module.css"

const CarpoolList = () => {
    const {carpoolList, isUpdating} = useContext(CarpoolContext)
    const [filteredCarpools, setFilteredCarpools] = useState([])

    useEffect(() => {
        const filteredList = carpoolList.filter(carpool => {
            if (carpool.hasOwnProperty("match")) return carpool.match
            return true
        })

        setFilteredCarpools(filteredList)
    }, [carpoolList])

    if (isUpdating) return (
        <div className={"d-flex justify-content-center pt-5"}>
            <Spinner animation={"border"}/>
        </div>
    )

    if (!Array.isArray(filteredCarpools) || !filteredCarpools.length) return (
        <h4 className={"text-muted text-center mt-5"}>Keine Fahrgemeinschaften verfügbar</h4>
    )

    return (
        <div className={styles.carpoolList}>
            {filteredCarpools.map(carpool =>
                <CarpoolListDetail carpool={carpool} key={carpool.id}/>
            )}
        </div>
    )
}

export default CarpoolList