import {Spinner} from "react-bootstrap";

const LoadingSpinner = ({isLoading, children}) => {
    if (isLoading) return <>
        <Spinner
            animation={"border"}
            as={"span"}
            role={"status"}
            className={"mr-3"}
            style={{width: "1.25em", height: "1.25em"}}
        />Loading...
    </>

    return children

}

export default LoadingSpinner