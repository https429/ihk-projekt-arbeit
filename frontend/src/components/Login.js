import React, {useContext, useState} from "react"
import "./Login.css"
import {Alert, Button, Form} from "react-bootstrap"
import {useForm} from "react-hook-form"
import {UserContext} from "../contextProvider/UserProvider"
import LoadingSpinner from "./LoadingSpinner"

const Login = () => {
    const {register, handleSubmit} = useForm()
    const [isFormValid, setFormValid] = useState(true)
    const [isLoading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const {login} = useContext(UserContext)

    const onSubmit = ({username, password}) => {
        setLoading(true)
        setError(null)
        login(username, password)
            .then(valid => {
                setLoading(false)
                setFormValid(valid)
            }).catch(err => {
            setLoading(false)
            setFormValid(true)
            setError(err)
        })
    }

    return (
        <div className={"login"}>
            <h1 id={"bx-drive-logo"}>BX-Drive</h1>
            <h3 className={"mb-3 text-center"}>Anmelden</h3>
            <Form
                id={"signIn"}
                onSubmit={handleSubmit(onSubmit)}
            >
                <fieldset disabled={isLoading}>
                    {!isFormValid
                    && <Alert variant={"danger"} className={"text-center"}>Username oder Passwort falsch</Alert>
                    }
                    {error
                    && <Alert variant={"danger"} className={"text-center"}>{error.toString()}</Alert>
                    }
                    <Form.Control
                        type={"text"}
                        id={"inputUsername"}
                        placeholder={"Username"}
                        required
                        // defaultValue={"user"}
                        {...register("username", {required: true})}
                    />
                    <Form.Control
                        type={"password"}
                        id={"inputPassword"}
                        placeholder={"Passwort"}
                        required
                        // defaultValue={"password"}
                        {...register("password", {required: true})}
                    />
                    <Button
                        type={"submit"}
                        variant={"primary"}
                        className={"btn-lg btn-block"}
                    >
                        <LoadingSpinner isLoading={isLoading}>Anmelden</LoadingSpinner>
                    </Button>
                    <p className={"mt-2 mb-3 text-muted text-center"}>Nutze zum Anmelden bitte deine BREDEX
                        Anmeldedaten.</p>
                </fieldset>
            </Form>
        </div>
    )
};

export default Login
