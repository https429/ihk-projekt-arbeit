import React, {useEffect, useState} from "react"
import {Layer, Source} from "react-map-gl";
import polyline from '@mapbox/polyline'

const Route = ({id, selected, geometry}) => {
    const defaultColor = '#343a40'
    const selectedColor = '#9400d3'
    const [geoJson, setGeoJson] = useState();

    useEffect(() => {
        setGeoJson(polyline.toGeoJSON(geometry))
    }, [selected, geometry])

    if (geoJson) return (
        <Source id={`carpool_${id}`} type="geojson" data={geoJson}>
            <Layer
                id={`route_${id}`}
                type={"line"}
                layout={{
                    'line-join': 'round',
                    'line-cap': 'round',
                }}
                paint={{
                    'line-color': (selected ? selectedColor : defaultColor),
                    'line-width': 5,
                    'line-opacity': (selected ? 1 : .5),
                }}
            />
        </Source>
    )

    return null
}

export default Route