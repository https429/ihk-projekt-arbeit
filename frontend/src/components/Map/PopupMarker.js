import React, {useContext, useEffect, useState} from "react"
import {Marker, Popup} from "react-map-gl";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import styles from "./PopupMarker.module.css"
import {Button} from "react-bootstrap";
import {CarpoolContext} from "../../contextProvider/CarpoolProvider";

const PopupMarker = ({label, address, filter}) => {
    const [showPopup, togglePopup] = useState(false)
    const {carpoolFilterQuery, setCarpoolFilterQuery} = useContext(CarpoolContext)

    useEffect(() => {
        const listener = event => {
            if (event.key === "Escape") togglePopup(false)
        }
        window.addEventListener("keydown", listener)
        return () => window.removeEventListener("keydown", listener)
    }, [])

    const toggleFilter = event => {
        if (carpoolFilterQuery === label) {
            setCarpoolFilterQuery("")
        } else {
            setCarpoolFilterQuery(label)
        }
    }

    return (<>
        <Marker
            longitude={address.longitude}
            latitude={address.latitude}
            offsetTop={-15}
        >
            <FontAwesomeIcon
                icon={["fas", "map-marker-alt"]}
                size={"2x"}
                color={"#343a40"}
                onClick={() => togglePopup(true)}
                className={styles.mapMarker}
            />
        </Marker>

        {showPopup && <Popup
            longitude={address.longitude}
            latitude={address.latitude}
            onClose={() => togglePopup(false)}
            closeOnClick={false}
            offsetLeft={12}
            offsetTop={12}
            tipSize={20}
        >
            <div className={styles.popupContent}>
                <p className={"font-weight-bold mr-4"}>{label}</p>
                <p>{address.streetName} {address.streetNumber}</p>
                <p>{address.postalCode} {address.city}</p>
                {filter && <Button
                    variant={label === carpoolFilterQuery ? 'dark' : 'outline-dark'}
                    onClick={toggleFilter}
                    className={"mt-2 btn-block"}
                >{label === carpoolFilterQuery ? 'Filter entfernen' : 'Filter setzten'}
                </Button>}
            </div>
        </Popup>}
    </>)
}

export default PopupMarker