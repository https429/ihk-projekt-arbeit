import React, {useContext, useEffect, useMemo, useState} from "react"
import "mapbox-gl/dist/mapbox-gl.css"
import "./Map.css"
import MapGL, {FlyToInterpolator, NavigationControl, WebMercatorViewport} from "react-map-gl"
import Route from "./Route"
import {CarpoolContext} from "../../contextProvider/CarpoolProvider"
import {UserContext} from "../../contextProvider/UserProvider"
import PopupMarker from "./PopupMarker"

const navControlStyle = {
    bottom: 30,
    right: 15,
}

const defaultViewport = {
    latitude: 52.26594,
    longitude: 10.52673,
    zoom: 12,
    maxPitch: 0,
    maxZoom: 17,
    minZoom: 7,
}

const Map = () => {
    const {carpoolList, selectedCarpool} = useContext(CarpoolContext)
    const {userObj} = useContext(UserContext)
    const [viewport, setViewport] = useState(defaultViewport)
    const [transitionDuration, setTransitionDuration] = useState(0)

    useEffect(() => {
        setTransitionDuration('auto')
        if (selectedCarpool) {
            setViewport(prevViewport => {
                const {destination, driver: {address}} = selectedCarpool
                const {longitude, latitude, zoom} = new WebMercatorViewport(prevViewport)
                    .fitBounds([[address.longitude, address.latitude], [destination.longitude, destination.latitude]], {
                        padding: 100,
                        maxZoom: 12,
                    });

                return {
                    ...prevViewport,
                    longitude,
                    latitude,
                    zoom,
                }
            })
        } else {
            setViewport(defaultViewport)
        }
    }, [selectedCarpool])

    const routes = useMemo(() => carpoolList.map(carpool => {
        if (carpool.match === false) return null

        return (<Route
            key={carpool.id}
            id={carpool.id}
            selected={carpool === selectedCarpool}
            geometry={carpool.geometry}
        />)
    }), [carpoolList, selectedCarpool])

    const markers = useMemo(() => {
        const markedDestinations = []
        return carpoolList.map(({destination, id, match}) => {
            if (match === false) return null

            const coords = `${destination.longitude};${destination.latitude}`
            if (!markedDestinations.includes(coords)) {
                markedDestinations.push(coords)
                return <PopupMarker
                    key={id}
                    label={destination.label}
                    address={destination}
                    filter={true}
                />
            }
            return null
        })
    }, [carpoolList])

    return (
        <MapGL
            {...viewport}
            width={"100%"}
            height={"100%"}
            mapStyle={"mapbox://styles/mapbox/light-v10"}
            onViewportChange={nextViewport => setViewport(nextViewport)}
            transitionDuration={transitionDuration}
            onTransitionEnd={() => setTransitionDuration(0)}
            transitionInterpolator={new FlyToInterpolator()}
        >
            <NavigationControl
                style={navControlStyle}
                showCompass={false}
            />
            {userObj?.address &&
                <PopupMarker
                    label={userObj.displayName}
                    address={userObj.address}
                    filter={false}
                />
            }

            {routes}
            {markers}
        </MapGL>
    )
}

export default Map