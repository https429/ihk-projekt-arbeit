export async function fetchJson(path, options = {}) {
    const url = `${process.env.REACT_APP_BASE_URL}${path}`

    return await fetch(url, {
        credentials: 'include',
        headers: {
            Accept: 'application/json',
        },
        ...options,
    })
}

export async function sendJson(method, path, payload = {}, options = {}) {
    const url = `${process.env.REACT_APP_BASE_URL}${path}`
    return await fetch(url, {
        method: method,
        credentials: 'include',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
        ...options,
    })
}