import React, {useContext} from "react"
import Login from "./components/Login"
import {UserContext} from "./contextProvider/UserProvider"
import Screen from "./components/Screen"

const App = () => {
    const {isAuthenticated} = useContext(UserContext)

    if (isAuthenticated) return <Screen/>
    return <Login/>
}

export default App