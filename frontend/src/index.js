import React from "react"
import ReactDOM from "react-dom"

import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css"
import "mapbox-gl/dist/mapbox-gl.css"
import "./index.css"
import {library} from "@fortawesome/fontawesome-svg-core"
import {fas} from "@fortawesome/free-solid-svg-icons"
import UserProvider from "./contextProvider/UserProvider";

library.add(fas)

ReactDOM.render(
    <React.StrictMode>
        <UserProvider>
            <App/>
        </UserProvider>
    </React.StrictMode>,
    document.getElementById("root")
)