UPDATE USER SET
display_name='Dina Kaczka',
email='dina.kaczka@bredex.de'
WHERE username='user';

UPDATE USER SET
display_name='Harm Meyer-Borstel',
email='harm.meyer-borstel@bredex.de'
WHERE username='harm';

UPDATE USER SET
display_name='Thilo Althaus',
email='thilo@althaus@bredex.de'
WHERE username='thilo';

UPDATE USER SET
display_name='Annett Klinger',
email='annett.klinger@bredex.de'
WHERE username='annett';

UPDATE USER SET
display_name='Giesela Hemmer',
email='giesela.hemmer@bredex.de'
WHERE username='giesela';

UPDATE USER SET
display_name='Marie-Theresa Kröner',
email='marie-theresa.kroener@bredex.de'
WHERE username='marie-theresa';

SELECT * FROM USER;