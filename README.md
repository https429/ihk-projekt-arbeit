Projekt Arbeit ~ Harm Meyer-Borstel
===================================

Dies ist meine Projektarbeit, welche im Zuge meiner Ausbildung 2020 programmiert habe.

Ziel der Anwendung war es den Mitarbeitenden zu ermöglichen sich in Fahrgemeinschaften zu organisieren und somit CO₂
einzusparen.

> **Meine fertige [Projektdokumentation](Projektdokumentation.pdf) ist übrigens auch in diesem Projekt**

<p align="center">
<img src="https://upload.wikimedia.org/wikipedia/en/3/30/Java_programming_language_logo.svg" alt="Java" height="50"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/7/79/Spring_Boot.svg" alt="Spring Boot" height="50"/>
<img src="https://upload.wikimedia.org/wikipedia/en/d/dd/MySQL_logo.svg" alt="MySQL" height="50"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" alt="React" height="50"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/b/b2/Bootstrap_logo.svg" alt="Bootstrap CSS" height="50"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/1/1f/Mapbox_logo_2019.svg" alt="Mapbox API" height="50"/>
</p>


## Preview
### Videos

- [Fahrgemeinschaft Finden](docs/BX-DriveFahrgemeinschaftFinden.mp4)
- [Fahrgemeinschaft Anlegen](docs/BX-DriveFahrgemeinschaftAnlegen.mp4)

![Screenshot vom Dashboard der Anwendung](docs/Screenshot_Dashboard.png)

## Projekt Einleitungstext

#### Entwicklung einer Webanwendung zur Organisation von Fahrgemeinschaften innerhalb eines Unternehmens

Aufgrund der wachsenden Bedrohung durch den Klimawandel ist nicht nur die Bevölkerung,
sondern sind auch die Unternehmen in der Verantwortung, ihren ökologischen Fußabdruck zu
verringern. Im Zuge dessen hat auch die BREDEX GmbH im Sommer 2020 ein
Nachhaltigkeitsprogramm gestartet. Neben der Vermeidung von Müll und dessen Trennung ist
auch die Mobilität ein großes Thema beim Klimaschutz. So wurden beispielsweise zwei
Elektrofahrzeuge als Dienstwagen angeschafft und der Umstieg auf das Fahrrad oder öffentliche
Verkehrsmittel durch eine Subvention des Arbeitgebers attraktiver gestaltet.

Für einige Mitarbeitende ist der Individualverkehr dennoch die bevorzugte Wahl des
Transportes, da öffentliche Verkehrsmittel teilweise schlecht zu erreichen sind oder zu lange
Fahrzeiten haben. Pro Fahrzeug werden oft nur ein bis zwei Personen transportiert. Um die
verfügbaren Platzkapazitäten in den Autos effizienter nutzen zu können, Kosten zu sparen und
die Umwelt zu schonen, empfiehlt sich die Gründung von Fahrgemeinschaften.

Ab einer gewissen Unternehmensgröße gestaltet sich die Absprache mit Kolleg∗innen aus
anderen Abteilungen schwierig. Auch für neue Mitarbeitende ist die Vernetzung mit
Kolleg∗innen aus der Umgebung problematisch, da Informationen zu Wohnorten oder
bestehende Fahrgemeinschaften fehlen beziehungsweise nicht kommuniziert werden.

Diese Problematik soll mit einer Anwendung gelöst werden. Auf einer freiwilligen Basis sollen
Mitarbeitende die Möglichkeit erhalten, ihren Wohnort sowie Fahrtziel in eine Datenbank
einpflegen zu können. Des Weiteren sollen die Nutzenden wählen, ob eine Fahrgemeinschaft
gesucht oder angeboten wird. Um die Privatsphäre der Teilnehmenden zu schützen, sollen den
Suchenden nur Fahrgemeinschaften angezeigt werden, die in einem Umkreis von zwei
Kilometern starten oder vorbeifahren. Zusätzlich werden Fahrgemeinschaften herausgefiltert,
die nicht zu demselben Standort fahren. Beim Erstellen eines Angebotes oder einer Suche
können die Nutzenden aus einer Liste den gewünschten Unternehmensstandort auswählen.

Die Oberfläche der Anwendung soll eine interaktive Karte sein. Auf dieser werden die Standorte
des Unternehmens sowie der eingetragene Wohnort als Marker dargestellt. Die gefilterten
Fahrgemeinschaften werden als Routen zwischen Startpunkt und Firmenstandort angezeigt.
