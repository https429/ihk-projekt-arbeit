package de.bredex.bxdrive.dao;

import de.bredex.bxdrive.model.Carpool;
import de.bredex.bxdrive.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarpoolRepository extends CrudRepository<Carpool, Long> {
    Iterable<Carpool> findAllByDriver(User user);

    Iterable<Carpool> findAllByDriverIsNotAndActiveTrue(User user);

    Iterable<Carpool> findAllByActiveTrue();
}
