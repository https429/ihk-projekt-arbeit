package de.bredex.bxdrive.dao;

import de.bredex.bxdrive.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {
    Optional<Address> findAddressByStreetNameAndStreetNumberAndPostalCodeAndCity(String streetName, String streetNumber, String postalCode, String city);

    Iterable<Address> findAddressesByLabelIsNotNull();
}
