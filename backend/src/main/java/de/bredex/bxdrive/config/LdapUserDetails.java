package de.bredex.bxdrive.config;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.naming.directory.Attributes;
import java.util.Collection;

public class LdapUserDetails implements UserDetails {
    private final Attributes attributes;
    private final String username;
    private final String password;
    private final Collection<? extends GrantedAuthority> authorities;

    public LdapUserDetails(DirContextOperations ctx, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this.attributes = ctx.getAttributes();
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
