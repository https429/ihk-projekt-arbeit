package de.bredex.bxdrive.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Slf4j
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Value("${ldap.url}")
    private String ldapUrl;
    @Value("${ldap.domain}")
    private String ldapDomain;
    @Value("${ldap.use}")
    private boolean useLdap;
    @Value("${inMemoryUser.username:user}")
    private String username;
    @Value("${inMemoryUser.password:password}")
    private String password;

    @SuppressWarnings("EmptyMethod")
    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and().csrf().disable()
                .headers().frameOptions().sameOrigin()

                .and().authorizeRequests()
                .antMatchers("/api/v1/login").permitAll()
                .antMatchers("/api/v1/**").fullyAuthenticated()
                .antMatchers("/**").permitAll()

                .and().exceptionHandling()
                .authenticationEntryPoint(((request, response, authException) -> response.sendError(UNAUTHORIZED.value(), UNAUTHORIZED.getReasonPhrase())));
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        if (this.useLdap) {
            ActiveDirectoryLdapAuthenticationProvider provider = new ActiveDirectoryLdapAuthenticationProvider(ldapDomain, ldapUrl);

            provider.setSearchFilter("(&(objectClass=user)(sAMAccountName={1}))");
            provider.setConvertSubErrorCodesToExceptions(true);
            provider.setUseAuthenticationRequestCredentials(false);
            provider.setUserDetailsContextMapper(new LdapUserDetailsContextMapper());
            auth.authenticationProvider(provider);
            log.info("Authentication via Ldap Server at {}", ldapUrl);
        } else {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            auth.inMemoryAuthentication()
                    .passwordEncoder(bcrypt)
                    .withUser(username).password(bcrypt.encode(password)).roles("USER")
                    .and().withUser("harm").password(bcrypt.encode(password)).roles("USER")
                    .and().withUser("thilo").password(bcrypt.encode(password)).roles("USER")
                    .and().withUser("annett").password(bcrypt.encode(password)).roles("USER")
                    .and().withUser("giesela").password(bcrypt.encode(password)).roles("USER")
                    .and().withUser("marie-theresa").password(bcrypt.encode(password)).roles("USER");
            log.info("Created in memory user with username: '{}' and password '{}'", username, password);
        }
    }
}
