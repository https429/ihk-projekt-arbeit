package de.bredex.bxdrive.service;

import de.bredex.bxdrive.dao.AddressRepository;
import de.bredex.bxdrive.dto.AddressDTO;
import de.bredex.bxdrive.exception.ResourceNotFoundException;
import de.bredex.bxdrive.model.Address;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.Optional;

@Slf4j
@Validated
@Service
public class AddressService {
    private final AddressRepository addressRepository;

    private final WebClient.Builder webClientBuilder;
    private final String API_TOKEN;

    @Autowired
    public AddressService(AddressRepository addressRepository, WebClient.Builder webClientBuilder, @Value("${mapbox.api-token}") String apiToken) {
        this.addressRepository = addressRepository;
        this.webClientBuilder = webClientBuilder;
        API_TOKEN = apiToken;
    }

    public Address findById(Long id) {
        return addressRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, "address"));
    }

    public Iterable<Address> findAllWithLabel() {
        return addressRepository.findAddressesByLabelIsNotNull();
    }

    public Address saveOrGet(@Valid AddressDTO dto) {
        Optional<Address> maybeAddress = addressRepository.findAddressByStreetNameAndStreetNumberAndPostalCodeAndCity(
                dto.getStreetName(), dto.getStreetNumber(), dto.getPostalCode(), dto.getCity());
        if (maybeAddress.isPresent()) {
            return maybeAddress.get();
        }

        double[] coords = fetchCoords(dto);
        Address address = new Address(
                dto.getLabel(),
                dto.getStreetName(),
                dto.getStreetNumber(),
                dto.getPostalCode(),
                dto.getCity(),
                coords[0],
                coords[1]
        );

        return addressRepository.save(address);
    }

    public double[] fetchCoords(AddressDTO dto) {
        URI uri = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api.mapbox.com")
                .path(String.format("geocoding/v5/mapbox.places/%s.json", dto.toString()))
                .queryParam("country", "de")
                .queryParam("types", "address")
                .queryParam("fuzzyMatch", "false")
                .queryParam("access_token", API_TOKEN)
                .build().toUri();

        log.info("Mapbox Geocoding request for {} with url: {}", dto, uri);
        String response = webClientBuilder.build()
                .get()
                .uri(uri)
                .retrieve()
                .bodyToMono(String.class)
                .block();

        JSONObject place = new JSONObject(response)
                .getJSONArray("features")
                .getJSONObject(0);

        if (!place.getString("place_name").contains(dto.toString())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bitte gib eine reale Addresse ein.");
        }

        double longitude = place.getJSONArray("center").getDouble(0);
        double latitude = place.getJSONArray("center").getDouble(1);
        log.info("Mapbox Geocoding request done! Long: {} Lat: {}", longitude, latitude);

        return new double[]{longitude, latitude};
    }

    public String fetchRoute(Address start, Address destination) {
        StringBuilder coords = new StringBuilder()
                .append(start.getLongitude()).append(',')
                .append(start.getLatitude()).append(';')
                .append(destination.getLongitude()).append(',')
                .append(destination.getLatitude());

        URI uri = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host("api.mapbox.com")
                .path(String.format("directions/v5/mapbox/driving/%s", coords))
                .queryParam("alternatives", false)
                .queryParam("steps", false)
                .queryParam("geometries", "polyline")
                .queryParam("overview", "full")
                .queryParam("access_token", API_TOKEN)
                .build().toUri();

        log.info("Mapbox Directions request for {} to {} with url: {}", start, destination, uri);
        String response = webClientBuilder.build()
                .get()
                .uri(uri)
                .retrieve()
                .bodyToMono(String.class)
                .block();

        String geometry = new JSONObject(response)
                .getJSONArray("routes")
                .getJSONObject(0)
                .getString("geometry");

        log.info("Mapbox Directions request done!");
        return geometry;
    }
}
