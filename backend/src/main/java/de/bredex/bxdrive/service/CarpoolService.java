package de.bredex.bxdrive.service;

import de.bredex.bxdrive.dao.CarpoolRepository;
import de.bredex.bxdrive.exception.ResourceNotFoundException;
import de.bredex.bxdrive.model.Carpool;
import de.bredex.bxdrive.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Validated
@Service
public class CarpoolService {
    private final CarpoolRepository carpoolRepository;

    @Autowired
    public CarpoolService(CarpoolRepository carpoolRepository) {
        this.carpoolRepository = carpoolRepository;
    }

    public List<Carpool> findAllForUser(User user) {
        return StreamSupport.stream(carpoolRepository.findAllByDriver(user).spliterator(), false)
                .collect(Collectors.toList());
    }

    public List<Carpool> findAllActiveExcludeUser(User user) {
        return StreamSupport.stream(carpoolRepository.findAllByDriverIsNotAndActiveTrue(user).spliterator(), false)
                .collect(Collectors.toList());
    }

    public Carpool save(Carpool carpool) {
        return carpoolRepository.save(carpool);
    }

    public List<Carpool> saveAll(Iterable<Carpool> carpools) {
        return StreamSupport.stream(carpoolRepository.saveAll(carpools).spliterator(), false)
                .collect(Collectors.toList());
    }

    public Carpool findById(Long id) {
        return carpoolRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, "carpool"));
    }

    public void delete(Long id) {
        carpoolRepository.deleteById(id);
    }

}
