package de.bredex.bxdrive.service;

import de.bredex.bxdrive.config.LdapUserDetails;
import de.bredex.bxdrive.dao.AddressRepository;
import de.bredex.bxdrive.dao.UserRepository;
import de.bredex.bxdrive.dto.UserDTO;
import de.bredex.bxdrive.exception.ResourceNotFoundException;
import de.bredex.bxdrive.exception.UnauthorizedActionException;
import de.bredex.bxdrive.model.User;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import java.security.Principal;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;

    @Autowired
    public UserService(UserRepository userRepository, AddressRepository addressRepository) {
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
    }

    public String getCurrentUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || authentication.getPrincipal() == null) {
            return null;
        }


        if (authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }

        if (authentication.getPrincipal() instanceof AuthenticatedPrincipal) {
            return ((AuthenticatedPrincipal) authentication.getPrincipal()).getName();
        }

        if (authentication.getPrincipal() instanceof Principal) {
            return ((Principal) authentication.getPrincipal()).getName();
        }

        return authentication.getPrincipal().toString();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @SneakyThrows({NamingException.class})
    public User getCurrentUser() {
        String name = getCurrentUserName();
        String displayName = name;
        String email = "user@fuck.off";
        if (name == null) {
            throw new UnauthorizedActionException();
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof LdapUserDetails) {
            Attributes attributes = ((LdapUserDetails) authentication.getPrincipal()).getAttributes();

            displayName = (String) attributes.get("displayName").get();
            email = (String) attributes.get("mail").get();
        }

        Optional<User> maybeUser = userRepository.findById(name);
        if (maybeUser.isPresent()) {
            User user = maybeUser.get();
            boolean updated = false;

            // Hat sich der DisplayName seit dem letzten mal geändert
//            if (user.getDisplayName() == null || !user.getDisplayName().equals(displayName)) {
//                user.setDisplayName(displayName);
//                updated = true;
//            }
//
//            if (user.getEmail() == null || !user.getEmail().equals(email)) {
//                user.setEmail(email);
//                updated = true;
//            }

            if (updated) {
                user = userRepository.save(user);
            }
            return user;
        }

        User user = new User(name);
        user.setDisplayName(displayName);
        user.setEmail(email);
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findById(username)
                .orElseThrow(() -> new ResourceNotFoundException(username, "user"));
    }

    public User update(@NonNull UserDTO dto) {
        return userRepository
                .findById(dto.getUserName())
                .map(user -> {
                    dto.copyToEntity(user, this.addressRepository);
                    user = userRepository.save(user);

                    return user;
                })
                .orElseThrow(() -> new ResourceNotFoundException(dto.getUserName(), "user"));
    }
}
