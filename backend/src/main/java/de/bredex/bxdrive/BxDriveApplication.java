package de.bredex.bxdrive;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class BxDriveApplication implements ApplicationRunner  {

	@Bean
	public WebClient.Builder getWebClientBuilder() {
		return WebClient.builder();
	}

	public static void main(String[] args) {
		SpringApplication.run(BxDriveApplication.class, args);
	}


	@SuppressWarnings("RedundantThrows")
	@Override
	public void run(ApplicationArguments args) throws Exception {

	}
}
