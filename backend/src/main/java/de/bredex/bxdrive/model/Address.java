package de.bredex.bxdrive.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

import static javax.persistence.GenerationType.IDENTITY;

@Table(
        name = "address",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "uk_address",
                        columnNames = {"street_name", "street_number", "postal_code", "city"}
                )
        }
)
@Entity
@Data
@NoArgsConstructor
public class Address {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "label", length = 50)
    private String label;

    @Column(name = "street_name", nullable = false, length = 50)
    private String streetName;

    @Pattern(regexp = "\\d+\\w?")
    @Column(name = "street_number", nullable = false, length = 4)
    private String streetNumber;

    @Pattern(regexp = "\\d{5}")
    @Column(name = "postal_code", nullable = false, length = 5)
    private String postalCode;

    @Column(name = "city", nullable = false, length = 50)
    private String city;

    @Column
    private Double longitude;
    @Column
    private Double latitude;

    public Address(String label, String street, String streetNumber, String postalCode, String city, Double longitude, Double latitude) {
        this.label = label;
        this.streetName = street;
        this.streetNumber = streetNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
