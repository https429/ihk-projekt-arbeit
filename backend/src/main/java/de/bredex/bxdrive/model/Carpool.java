package de.bredex.bxdrive.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Table(
        name = "carpool",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "uk_carpool_destination_driver",
                        columnNames = {"destination_id", "driver_username"}
                        )
        }
)
@Entity
@Data
@NoArgsConstructor
public class Carpool {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(nullable = false, updatable = false, insertable = false)
    private Long id;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "geometry", length = 30000)
    private String geometry;
    @ManyToOne
    @JoinColumn(
            name = "destination_id",
            nullable = false
    )
    private Address destination;
    @ManyToOne
    @JoinColumn(
            name = "driver_username",
            nullable = false,
            updatable = false
    )
    private User driver;

    public Carpool(boolean active, String geometry, Address destination, User driver) {
        this.active = active;
        this.geometry = geometry;
        this.destination = destination;
        this.driver = driver;
    }
}
