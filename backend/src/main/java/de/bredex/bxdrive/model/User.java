package de.bredex.bxdrive.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@Table(
        name = "user"
)
@Entity
@Data
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "username", nullable = false, updatable = false, length = 50)
    private String username;
    @Column(name = "display_name", length = 50)
    private String displayName;
    @Email
    @Column(name = "email", length = 75)
    private String email;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(mappedBy = "driver", orphanRemoval = true)
    private List<Carpool> carpoolList;

    public User(String username) {
        this.username = username;
    }
}
