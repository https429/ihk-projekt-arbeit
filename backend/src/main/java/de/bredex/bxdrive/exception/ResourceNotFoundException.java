package de.bredex.bxdrive.exception;

import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

public class ResourceNotFoundException extends ResponseStatusException {
    public ResourceNotFoundException(Long id, String object) {
        this(id.toString(), object);
    }

    public ResourceNotFoundException(String id, String object) {
        super(NOT_FOUND, String.format("Ops! Can not find %s with ID: %s", object, id));
    }
}
