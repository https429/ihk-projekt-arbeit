package de.bredex.bxdrive.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class InternalServerErrorException extends ResponseStatusException {

    public InternalServerErrorException() {
        super(INTERNAL_SERVER_ERROR, "Ops! Something went wrong.");
    }
}
