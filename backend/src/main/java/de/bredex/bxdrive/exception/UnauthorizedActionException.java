package de.bredex.bxdrive.exception;

import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.FORBIDDEN;

public class UnauthorizedActionException extends ResponseStatusException {
    public UnauthorizedActionException() {
        super(FORBIDDEN);
    }
}
