package de.bredex.bxdrive.controller;

import de.bredex.bxdrive.dto.AddressDTO;
import de.bredex.bxdrive.dto.UserDTO;
import de.bredex.bxdrive.model.Address;
import de.bredex.bxdrive.model.Carpool;
import de.bredex.bxdrive.model.User;
import de.bredex.bxdrive.service.AddressService;
import de.bredex.bxdrive.service.CarpoolService;
import de.bredex.bxdrive.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;
    private final AddressService addressService;
    private final CarpoolService carpoolService;

    @Autowired
    public UserController(UserService userService, AddressService addressService, CarpoolService carpoolService) {
        this.userService = userService;
        this.addressService = addressService;
        this.carpoolService = carpoolService;
    }

    @GetMapping("{username}")
    UserDTO getUserByUsername(@PathVariable String username) {
        return new UserDTO(userService.findByUsername(username));
    }

    @PutMapping
    UserDTO updateUser(@Valid @RequestBody AddressDTO addressDTO) throws IOException, InterruptedException {
        User user = userService.getCurrentUser();
        Address address = addressService.saveOrGet(addressDTO);

        for (Carpool carpool :
                user.getCarpoolList()) {
            carpool.setGeometry(addressService.fetchRoute(user.getAddress(), carpool.getDestination()));
        }
        carpoolService.saveAll(user.getCarpoolList());

        user.setAddress(address);
        return new UserDTO(userService.update(new UserDTO(user)));
    }
}
