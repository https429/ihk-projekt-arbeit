package de.bredex.bxdrive.controller;

import de.bredex.bxdrive.dto.ActiveDTO;
import de.bredex.bxdrive.dto.AddressDTO;
import de.bredex.bxdrive.dto.CarpoolDTO;
import de.bredex.bxdrive.dto.SuccessDTO;
import de.bredex.bxdrive.exception.InternalServerErrorException;
import de.bredex.bxdrive.model.Address;
import de.bredex.bxdrive.model.Carpool;
import de.bredex.bxdrive.model.User;
import de.bredex.bxdrive.service.AddressService;
import de.bredex.bxdrive.service.CarpoolService;
import de.bredex.bxdrive.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/carpool")
public class CarpoolController {
    private final CarpoolService carpoolService;
    private final UserService userService;
    private final AddressService addressService;

    @Autowired
    public CarpoolController(CarpoolService carpoolService, UserService userService, AddressService addressService) {
        this.carpoolService = carpoolService;
        this.userService = userService;
        this.addressService = addressService;
    }

    @GetMapping
    List<CarpoolDTO> getAllCarpools(@RequestParam(required = false) boolean own) {
        if (own) {
            return carpoolService.findAllForUser(userService.getCurrentUser()).stream()
                    .map(CarpoolDTO::new)
                    .collect(Collectors.toList());
        }
        return carpoolService.findAllActiveExcludeUser(userService.getCurrentUser()).stream()
                .map(CarpoolDTO::new)
                .collect(Collectors.toList());
    }

    @PostMapping
    CarpoolDTO addCarpool(@Valid @RequestBody AddressDTO destinationDTO) throws IOException, InterruptedException {
        User user = userService.getCurrentUser();
        Address destination = addressService.saveOrGet(destinationDTO);

        Carpool carpool = new Carpool(
                true,
                addressService.fetchRoute(user.getAddress(), destination),
                destination,
                user);
        return new CarpoolDTO(carpoolService.save(carpool));
    }

    @GetMapping("{carpoolID}")
    CarpoolDTO getCarpoolByID(@PathVariable Long carpoolID) {
        return new CarpoolDTO(carpoolService.findById(carpoolID));
    }

    @DeleteMapping("{carpoolID}")
    SuccessDTO deleteCarpoolByID(@PathVariable Long carpoolID) {
        carpoolService.delete(carpoolID);

        return new SuccessDTO();
    }

    @PutMapping("{carpoolID}")
    CarpoolDTO changeStatus(@PathVariable Long carpoolID, @RequestBody ActiveDTO activeDTO) {
        Carpool carpool = carpoolService.findById(carpoolID);

        carpool.setActive(activeDTO.isActive());

        return new CarpoolDTO(carpoolService.save(carpool));
    }
}
