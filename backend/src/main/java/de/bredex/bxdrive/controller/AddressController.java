package de.bredex.bxdrive.controller;

import de.bredex.bxdrive.dto.AddressDTO;
import de.bredex.bxdrive.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/api/v1/address")
public class AddressController {
    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    List<AddressDTO> getAllAddressWithLabel() {
        return StreamSupport.stream(addressService.findAllWithLabel().spliterator(), false)
                .map(AddressDTO::new)
                .collect(Collectors.toList());
    }
}
