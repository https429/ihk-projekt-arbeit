package de.bredex.bxdrive.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import de.bredex.bxdrive.model.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressDTO {
    private Long id;
    private String label;
    private String streetName;
    private String streetNumber;
    private String postalCode;
    private String city;
    private Double longitude;
    private Double latitude;

    public AddressDTO(Address address) {
        this.id = address.getId();
        this.label = address.getLabel();
        this.streetName = address.getStreetName();
        this.streetNumber = address.getStreetNumber();
        this.postalCode = address.getPostalCode();
        this.city = address.getCity();
        this.longitude = address.getLongitude();
        this.latitude = address.getLatitude();
    }

    public void copyToEntity(Address address) {
        address.setLabel(this.label);
        address.setStreetName(this.streetName);
        address.setStreetNumber(this.streetNumber);
        address.setPostalCode(this.postalCode);
        address.setCity(this.city);
        address.setLongitude(this.longitude);
        address.setLatitude(this.latitude);
    }

    @Override
    public String toString() {
        return String.format("%s %s, %s %s", streetName, streetNumber, postalCode, city);
    }
}
