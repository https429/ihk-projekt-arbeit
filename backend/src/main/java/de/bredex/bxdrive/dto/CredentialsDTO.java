package de.bredex.bxdrive.dto;

import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

@Data
public class CredentialsDTO implements Serializable {
    @NonNull
    private String username = "";

    @NonNull
    private String password = "";
}
