package de.bredex.bxdrive.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.bredex.bxdrive.dao.AddressRepository;
import de.bredex.bxdrive.exception.ResourceNotFoundException;
import de.bredex.bxdrive.model.Address;
import de.bredex.bxdrive.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
public class UserDTO {
    private String userName;
    private String displayName;
    @Email
    private String email;
    private AddressDTO address;
    @JsonIgnore
    private Long addressID;

    public UserDTO(User user) {
        this.userName = user.getUsername();
        this.displayName = user.getDisplayName();
        this.email = user.getEmail();

        if (user.getAddress() != null) {
            this.addressID = user.getAddress().getId();
            this.address = new AddressDTO(user.getAddress());
        }
    }

    public void copyToEntity(User user, AddressRepository addressRepository) {

        Address address = null;
        if (this.addressID != null) {
            address = addressRepository.findById(this.addressID)
                    .orElseThrow(() -> new ResourceNotFoundException(this.addressID, "address"));
        }

        user.setUsername(this.userName);
        user.setDisplayName(this.displayName);
        user.setEmail(this.email);
        user.setAddress(address);
    }
}
