package de.bredex.bxdrive.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.bredex.bxdrive.model.Address;
import de.bredex.bxdrive.model.Carpool;
import de.bredex.bxdrive.model.User;
import de.bredex.bxdrive.service.AddressService;
import de.bredex.bxdrive.service.UserService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarpoolDTO {
    private Long id;
    private boolean active;
    private String geometry;
    @JsonProperty("driver")
    private UserDTO driverDTO;
    @JsonProperty("destination")
    private AddressDTO destinationDTO;

    @JsonIgnore
    private User driver;
    @JsonIgnore
    private Address destination;

    public CarpoolDTO(@NonNull Carpool carpool) {
        this.id = carpool.getId();
        this.active = carpool.isActive();
        this.geometry = carpool.getGeometry();

        this.driver = carpool.getDriver();
        this.destination = carpool.getDestination();

        this.driverDTO = new UserDTO(this.driver);
        this.destinationDTO = new AddressDTO(this.destination);
    }

    public void copyToEntity(Carpool carpool, UserService userService, AddressService addressService) {
        carpool.setActive(this.active);
        carpool.setGeometry(this.geometry);
        carpool.setDriver(userService.findByUsername(driver.getUsername()));
        carpool.setDestination(addressService.findById(id));
    }
}
